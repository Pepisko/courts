## Courts Project

-Ve složce diagrams jsou použité diagramy v UML formátu.
Class diagram byl vytvořen ještě před implementací a je tedy pouze návrhový.
ERD diagram je vytvořen z aktuální implementace a je tedy aktuální.

-Snažil jsem se kód dokumentovat, někdy je dokumentovaný až příliš, nevěděl jsem jak moc se to ode mně očekává.

-Testuji přímo samotné REST endpointy, věřím že se mi tím povedlo otestovat velkou část kódu

-V kódu jsou implementovány i věci navíc, například upráva uživatele jako sama sebe, a správa svojí již vytvořené
rezervace, endpointům jsem ale nakonec nedal přístupová práva, aby se to nedalo použít (chtěl jsem aby byl
úkol v souladu se zadáním).

-Implementoval jsem i další různé endpointy, které nebyly přímo v souladu se zadáním, ale které mi přišly důležité

-Kód by potřeboval vytvořit pořádné validátory, které by se staraly o validaci vstupů, ale to bych řekl, že je docela
nad rámec úkolu. Přístup, který tam funguje teď (funkce prostě vyhazují Exceptions) není úplně ideální. Zkoušel jsem
ještě vytvořit vlastní anotaci na validaci telefonního čísla, ale nedodělal jsem to.

-Ohledně security, která mi dala nejvíce zabrat jsem se inspiroval tímto projektem na GitHubu:
https://github.com/ali-bouali/spring-boot-3-jwt-security

-PS: omlouvám se, že testy jsou takové odfláklé, samotná implementace mi zabrala více času než se kterým jsem počítal
a s testy nemám moc zkušeností.

