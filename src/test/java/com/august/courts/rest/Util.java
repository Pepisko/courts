package com.august.courts.rest;

import com.august.courts.controller.dto.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class Util {

    public static final ObjectMapper mapper = new ObjectMapper()
            .registerModule(new ParameterNamesModule())
            .registerModule(new Jdk8Module())
            .registerModule(new JavaTimeModule());

    private Util() {
    }

    public static ResultActions login(LoginDTO loginDTO, MockMvc mvc) throws Exception {
        return mvc.perform(post("/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(loginDTO)));
    }

    public static String loginAndGetJwt(LoginDTO loginDTO, MockMvc mvc) throws Exception {
        var res = login(loginDTO, mvc).andReturn();
        var jwt = res.getResponse().getHeader(HttpHeaders.AUTHORIZATION);
        assertEquals(res.getResponse().getStatus(), HttpStatus.OK.value());
        return jwt;
    }

    public static ResultActions logout(String jwt, MockMvc mvc) throws Exception {
        return mvc.perform(post("/api/auth/logout")
                        .header("Authorization", jwt))
                .andExpect(status().isOk());
    }

    public static void registerUser(ApplicationUserDTO user, MockMvc mvc) throws Exception {
        mvc.perform(post("/api/auth/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(user)))
                .andExpect(status().isOk());
    }

    public static ResultActions createSurface(SurfaceDTO surface, String jwt, MockMvc mvc) throws Exception {
        return mvc.perform(post("/api/surface/create")
                .header("Authorization", jwt)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(surface)));
    }

    public static ResultActions createCourt(String surfaceName, String jwt, MockMvc mvc, Long id) throws Exception {
        var court = new CourtDTO(
                id,
                "court" + id,
                surfaceName
        );

        return mvc.perform(post("/api/court/create")
                .header(HttpHeaders.AUTHORIZATION, jwt)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(court)));
    }

    public static ResultActions createReservation(ReservationDTO reservationDTO, String jwt, MockMvc mvc) throws Exception {
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        mapper.setDateFormat(new StdDateFormat().withColonInTimeZone(true));
        return mvc.perform(post("/api/reservation/create")
                .header(HttpHeaders.AUTHORIZATION, jwt)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(reservationDTO)));
    }


}
