package com.august.courts.rest;


import com.august.courts.repositories.ApplicationUserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ApplicationUserTest {


    @Autowired
    private MockMvc mvc;

    @Autowired
    private ApplicationUserRepository applicationUserRepository;

    private static final String BASE_URI = "http://localhost:8080";

    @Test
    @Rollback(value = true)
    public void createUser_AsAdmin_Success() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);

        mvc.perform(post(BASE_URI + "/api/user/create")
                        .header(HttpHeaders.AUTHORIZATION, jwt)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Util.mapper.writeValueAsString(TestInputs.createRegistrationNumber(4))))
                .andExpect(status().isOk());

        Util.logout(jwt, mvc);
    }

    @Test
    public void createAdminUser_AsUser_Fail() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_USER_DEFAULT, mvc);

        mvc.perform(post(BASE_URI + "/api/user/create")
                        .header(HttpHeaders.AUTHORIZATION, jwt)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Util.mapper.writeValueAsString(TestInputs.REGISTRATION_ADMIN_2)))
                .andExpect(status().isForbidden());

        Util.logout(jwt, mvc);
    }

    @Test
    public void createAdminUser_MalformedJwt_Fail() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);

        mvc.perform(post(BASE_URI + "/api/user/create")
                        .header(HttpHeaders.AUTHORIZATION, "Be arerasfasfasfsss")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Util.mapper.writeValueAsString(TestInputs.createRegistrationNumber(5))))
                .andExpect(status().isUnauthorized());

        Util.logout(jwt, mvc);
    }

    @Test
    public void updateUser_ByPhone_AsAdmin_Success() throws Exception {

        var user = TestInputs.createRegistrationNumber(2);

        Util.registerUser(user, mvc);
        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);

        var res = mvc.perform(put(BASE_URI + "/api/user/update/phone=" + user.phoneNumber())
                .header(HttpHeaders.AUTHORIZATION, jwt)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Util.mapper.writeValueAsString(TestInputs.createRegistrationNumber(6))));

        res.andExpect(status().isOk());
        assertTrue(res.andReturn().getResponse().getContentAsString().contains("user6"));

        Util.logout(jwt, mvc);
    }

    @Test
    public void updateUser_AsUser_Fail() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_USER_DEFAULT, mvc);

        var res = mvc.perform(put(BASE_URI + "/api/user/update/phone=" + TestInputs.REGISTRATION_ADMIN_1.phoneNumber())
                .header(HttpHeaders.AUTHORIZATION, jwt)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Util.mapper.writeValueAsString(TestInputs.REGISTRATION_ADMIN_1)));

        res.andExpect(status().isForbidden());

        Util.logout(jwt, mvc);
    }

    @Test
    public void getAll_Success() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);

        var res = mvc.perform(get(BASE_URI + "/api/user/all")
                .header(HttpHeaders.AUTHORIZATION, jwt)
                .contentType(MediaType.APPLICATION_JSON));

        res.andExpect(status().isOk());
        assertEquals(
                applicationUserRepository.findAll().size(),
                Util.mapper.readTree(res.andReturn().getResponse().getContentAsString()).size()
        );

        Util.logout(jwt, mvc);
    }

    @Test
    public void getByPhone_Success() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);

        var res = mvc.perform(get(BASE_URI + "/api/user/get/phone=" + TestInputs.LOGIN_USER_DEFAULT.phoneNumber())
                .header(HttpHeaders.AUTHORIZATION, jwt)
                .contentType(MediaType.APPLICATION_JSON));

        res.andExpect(status().isOk());
        assertTrue(res.andReturn().getResponse().getContentAsString().contains(TestInputs.LOGIN_USER_DEFAULT.phoneNumber()));

        Util.logout(jwt, mvc);
    }

    @Test
    public void getNonExistent_Fail() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);

        var res = mvc.perform(get(BASE_URI + "/api/user/get/phone=" + TestInputs.createLoginNumber(7).phoneNumber())
                        .header(HttpHeaders.AUTHORIZATION, jwt)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        Util.logout(jwt, mvc);
    }

    @Test
    public void deleteByPhone_Success() throws Exception {

        Util.registerUser(TestInputs.createRegistrationNumber(8), mvc);

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);
        var numOfUsers = applicationUserRepository.findAll().size();

        var res = mvc.perform(delete(BASE_URI + "/api/user/delete/phone=" + TestInputs.createLoginNumber(8).phoneNumber())
                .header(HttpHeaders.AUTHORIZATION, jwt)
                .contentType(MediaType.APPLICATION_JSON));

        res.andExpect(status().isOk());
        assertEquals(numOfUsers - 1, applicationUserRepository.findAll().size());

        Util.logout(jwt, mvc);
    }

    @Test
    public void deleteNonExistent_Fail() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);

        mvc.perform(delete(BASE_URI + "/api/user/delete/phone=" + TestInputs.createLoginNumber(9).phoneNumber())
                        .header(HttpHeaders.AUTHORIZATION, jwt)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        Util.logout(jwt, mvc);
    }


}
