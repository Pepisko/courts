package com.august.courts.rest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class AuthTest {

    @Autowired
    private MockMvc mvc;

    private static final String BASE_URI = "http://localhost:8080";


    @Test
    public void registerApplicationUser_Success_ThenLogin_Success_ThenLogout_Success() throws Exception {
        mvc.perform(post(BASE_URI + "/api/auth/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Util.mapper.writeValueAsString(TestInputs.createRegistrationNumber(1))))
                .andExpect(status().isOk());

        var res = mvc.perform(post(BASE_URI + "/api/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Util.mapper.writeValueAsString(TestInputs.createLoginNumber(1))))
                .andReturn();

        var jwt = res.getResponse().getHeader(HttpHeaders.AUTHORIZATION);

        assertEquals(res.getResponse().getStatus(), HttpStatus.OK.value());

        Util.logout(jwt, mvc);
    }

    @Test
    public void registerWithoutMail_Success_ThenLogin_Success_ThenLogout_Success() throws Exception {
        mvc.perform(post(BASE_URI + "/api/auth/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Util.mapper.writeValueAsString(TestInputs.REGISTRATION_WITHOUT_MAIL)))
                .andExpect(status().isOk());

        var res = mvc.perform(post(BASE_URI + "/api/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Util.mapper.writeValueAsString(TestInputs.LOGIN_WITHOUT_MAIL)))
                .andReturn();

        var jwt = res.getResponse().getHeader(HttpHeaders.AUTHORIZATION);

        assertEquals(res.getResponse().getStatus(), HttpStatus.OK.value());

        Util.logout(jwt, mvc);

    }

    @Test
    public void registerApplicationUser_AlreadyRegistered_Fail() throws Exception {
        mvc.perform(post(BASE_URI + "/api/auth/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Util.mapper.writeValueAsString(TestInputs.REGISTRATION_ALREADY_EXIST_PHONE_NUMBER)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void registerEmptyPassword_Fail() throws Exception {
        mvc.perform(post(BASE_URI + "/api/auth/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Util.mapper.writeValueAsString(TestInputs.REGISTRATION_EMPTY_PASSWORD)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void registerInvalidPhone_Fail() throws Exception {
        mvc.perform(post(BASE_URI + "/api/auth/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Util.mapper.writeValueAsString(TestInputs.REGISTRATION_INVALID_PHONE_NUMBER)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void loginAdmin_Success() throws Exception {
        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);
        Util.logout(jwt, mvc);
    }

    @Test
    public void updateUser_AsSelf_AssignAdmin_Fail() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_USER_DEFAULT, mvc);

        mvc.perform(put(BASE_URI + "/api/auth/update/self")
                        .header(HttpHeaders.AUTHORIZATION, jwt)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Util.mapper.writeValueAsString(TestInputs.REGISTRATION_ADMIN_2)))
                .andExpect(status().isBadRequest());

        Util.logout(jwt, mvc);
    }

    @Test
    public void updateUser_Self_Success() throws Exception {

        Util.registerUser(TestInputs.createRegistrationNumber(2), mvc);
        var jwt = Util.loginAndGetJwt(TestInputs.createLoginNumber(2), mvc);

        var res = mvc.perform(put(BASE_URI + "/api/auth/update/self")
                .header(HttpHeaders.AUTHORIZATION, jwt)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Util.mapper.writeValueAsString(TestInputs.createRegistrationNumber(3))));

        res.andExpect(status().isOk());
        assertTrue(res.andReturn().getResponse().getContentAsString().contains("user3"));

    }


}
