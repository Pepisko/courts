package com.august.courts.rest;

import com.august.courts.controller.dto.ApplicationUserDTO;
import com.august.courts.controller.dto.LoginDTO;
import com.august.courts.controller.dto.ReservationDTO;
import com.august.courts.domain.RoleName;
import com.august.courts.domain.utils.GameType;
import com.august.courts.service.ApplicationUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.Set;


@RequiredArgsConstructor
@SpringBootTest
public class TestInputs {

    @Autowired
    private final ApplicationUserService applicationUserService;

    static final ApplicationUserDTO REGISTRATION_WITHOUT_MAIL = new ApplicationUserDTO(
            "+420222222212",
            "password",
            null,
            "user_without_mail",
            null
    );
    static final LoginDTO LOGIN_WITHOUT_MAIL = new LoginDTO(
            REGISTRATION_WITHOUT_MAIL.phoneNumber(),
            REGISTRATION_WITHOUT_MAIL.password()
    );

    static final ApplicationUserDTO REGISTRATION_ALREADY_EXIST_PHONE_NUMBER = new ApplicationUserDTO(
            "+420000000001",
            "password",
            "user@user.com",
            "user_existing_phone",
            null
    );

    static final ApplicationUserDTO REGISTRATION_EMPTY_PASSWORD = new ApplicationUserDTO(
            "+420444444404",
            null,
            "user@user.com",
            "user_empty_password",
            Set.of("USER")
    );

    static final ApplicationUserDTO REGISTRATION_INVALID_PHONE_NUMBER = new ApplicationUserDTO(
            "+4023m456789",
            "password",
            "user@user.com",
            "user_invalid_phone",
            Set.of("USER")
    );

    static final ApplicationUserDTO REGISTRATION_INVALID_ROLE = new ApplicationUserDTO(
            "+420666666606",
            "password",
            "user@user.com",
            "user_invalid_role",
            Set.of("INVALID_ROLE")
    );

    static final ApplicationUserDTO REGISTRATION_ADMIN_1 = new ApplicationUserDTO(
            "+420000000001",
            "password",
            "admin@admin.com",
            "admin1",
            Set.of("ADMIN")
    );

    static final ApplicationUserDTO REGISTRATION_ADMIN_2 = new ApplicationUserDTO(
            "+420000000003",
            "password",
            "admin@admin.com",
            "admin2",
            Set.of("ADMIN")
    );


    static final LoginDTO LOGIN_ADMIN_DEFAULT = new LoginDTO(
            "+420000000001",
            "admin"
    );


    static final LoginDTO LOGIN_USER_DEFAULT = new LoginDTO(
            "+420000000002",
            "user"
    );

    public static ApplicationUserDTO createRegistrationNumber(int number) {
        String phone = generatePhoneNumber(number);
        String name = "user" + number;
        String mail = name + "@user.com";

        return new ApplicationUserDTO(
                phone,
                "password",
                mail,
                name,
                Set.of(RoleName.USER));
    }

    public static LoginDTO createLoginNumber(int number) {
        return new LoginDTO(
                generatePhoneNumber(number),
                "password");
    }

    private static String generatePhoneNumber(int number) {
        if (number < 0 || number > 9) {
            throw new IllegalArgumentException("Number must be positive and lesser than 10");
        }

        return "+420" + String.valueOf(number).repeat(9);
    }

    public static ReservationDTO generateReservationNumber(int number, GameType gameType, Long courtId, String phoneNumber) {
        return new ReservationDTO(
                LocalDateTime.now().plusDays(2).plusHours(number + 1),
                LocalDateTime.now().plusDays(2).plusHours(number + 1).plusHours(2),
                phoneNumber,
                gameType,
                courtId
        );
    }
}
