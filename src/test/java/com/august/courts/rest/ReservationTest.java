package com.august.courts.rest;

import com.august.courts.domain.utils.GameType;
import com.august.courts.repositories.ReservationRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class ReservationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ReservationRepository reservationRepository;

    private static final String BASE_URI = "http://localhost:8080";


    @Test
    public void createReservation_AsUser_Success() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_USER_DEFAULT, mvc);

        var before = reservationRepository.getAllByApplicationUser_PhoneNumber(
                TestInputs.LOGIN_USER_DEFAULT.phoneNumber()
        ).size();

        var reservationDTO = TestInputs.generateReservationNumber(
                1, GameType.TWO_ON_TWO, 1L, TestInputs.LOGIN_USER_DEFAULT.phoneNumber()
        );

        var res = Util.createReservation(reservationDTO, jwt, mvc);
        res.andExpect(status().isOk());

        assertEquals(
                before + 1,
                reservationRepository
                        .getAllByApplicationUser_PhoneNumber(TestInputs.LOGIN_USER_DEFAULT.phoneNumber())
                        .size());

        Util.logout(jwt, mvc);
    }

    @Test
    public void createReservation_AsUser_Overlapping_Fail() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_USER_DEFAULT, mvc);

        var reservationDTO = TestInputs.generateReservationNumber(
                4, GameType.ONE_ON_ONE, 1L, TestInputs.LOGIN_USER_DEFAULT.phoneNumber()
        );

        var reservationDTO2 = TestInputs.generateReservationNumber(
                5, GameType.ONE_ON_ONE, 1L, TestInputs.LOGIN_USER_DEFAULT.phoneNumber()
        );

        var res = Util.createReservation(reservationDTO, jwt, mvc);
        res.andExpect(status().isOk());

        var res2 = Util.createReservation(reservationDTO2, jwt, mvc);
        res2.andExpect(status().isBadRequest());

        Util.logout(jwt, mvc);
    }

    @Test
    public void createReservation_AsUser_ForDifferentUser_Fail() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_USER_DEFAULT, mvc);

        var before = reservationRepository.getAllByApplicationUser_PhoneNumber(
                TestInputs.LOGIN_USER_DEFAULT.phoneNumber()
        ).size();

        var reservationDTO = TestInputs.generateReservationNumber(
                1, GameType.ONE_ON_ONE, 2L, TestInputs.LOGIN_ADMIN_DEFAULT.phoneNumber()
        );

        var res = Util.createReservation(reservationDTO, jwt, mvc);
        res.andExpect(status().isBadRequest());

        Util.logout(jwt, mvc);
    }

    @Test
    public void getReservationByCourt_AsUser_Success() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);

        var reservationDTO = TestInputs.generateReservationNumber(
                4, GameType.TWO_ON_TWO, 2L, TestInputs.LOGIN_ADMIN_DEFAULT.phoneNumber()
        );

        Util.createReservation(reservationDTO, jwt, mvc)
                .andExpect(status().isOk());

        var res = mvc.perform(get(BASE_URI + "/api/reservation/all/courtId=" + reservationDTO.courtId())
                .header(HttpHeaders.AUTHORIZATION, jwt)
                .contentType(MediaType.APPLICATION_JSON));

        res.andExpect(status().isOk());

        var reservationExpected = reservationRepository
                .getAllByApplicationUser_PhoneNumber(TestInputs.LOGIN_ADMIN_DEFAULT.phoneNumber())
                .get(0);


        var reservationActual = res.andReturn().getResponse().getContentAsString();
        assertTrue(reservationActual.contains(reservationExpected.getId().toString()));

        Util.logout(jwt, mvc);
    }

    @Test
    public void getReservationByPhone_AsUser_Success() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);

        var reservationDTO = TestInputs.generateReservationNumber(
                7, GameType.TWO_ON_TWO, 2L, TestInputs.LOGIN_ADMIN_DEFAULT.phoneNumber()
        );

        Util.createReservation(reservationDTO, jwt, mvc)
                .andExpect(status().isOk());

        var res = mvc.perform(get(
                BASE_URI + "/api/reservation/all/phone="
                        + TestInputs.LOGIN_ADMIN_DEFAULT.phoneNumber() + "/in-future=true"
        )
                .header(HttpHeaders.AUTHORIZATION, jwt)
                .contentType(MediaType.APPLICATION_JSON));

        res.andExpect(status().isOk());

        var reservationCountExpected = reservationRepository
                .getAllByApplicationUser_PhoneNumber(TestInputs.LOGIN_ADMIN_DEFAULT.phoneNumber())
                .size();

        assertEquals(
                reservationCountExpected,
                Util.mapper.readTree(res.andReturn().getResponse().getContentAsString()).size()
        );

        Util.logout(jwt, mvc);
    }

    @Test
    public void updateReservation_OtherUser_Fail() throws Exception {
        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);

        var reservationDTOtoCreate = TestInputs.generateReservationNumber(
                1, GameType.TWO_ON_TWO, 3L, TestInputs.LOGIN_ADMIN_DEFAULT.phoneNumber()
        );

        var created = Util.createReservation(reservationDTOtoCreate, jwt, mvc);
        created.andExpect(status().isOk());

        Util.logout(jwt, mvc);

        var jwt2 = Util.loginAndGetJwt(TestInputs.LOGIN_USER_DEFAULT, mvc);

        var id = Util.mapper.readTree(created.andReturn().getResponse().getContentAsString()).get("id");

        var reservationDTOtoUpdate = TestInputs.generateReservationNumber(
                2, GameType.TWO_ON_TWO, 3L, TestInputs.LOGIN_USER_DEFAULT.phoneNumber()
        );

        mvc.perform(put(BASE_URI + "/api/reservation/update/id=" + id)
                        .header(HttpHeaders.AUTHORIZATION, jwt2)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Util.mapper.writeValueAsString(reservationDTOtoUpdate)))
                .andExpect(status().isForbidden());

        Util.logout(jwt2, mvc);
    }

    @Test
    public void updateReservation_Success() throws Exception {
        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);

        var reservationDTOtoCreate = TestInputs.generateReservationNumber(
                5, GameType.TWO_ON_TWO, 3L, TestInputs.LOGIN_ADMIN_DEFAULT.phoneNumber()
        );

        var created = Util.createReservation(reservationDTOtoCreate, jwt, mvc);
        created.andExpect(status().isOk());

        var id = Util.mapper.readTree(created.andReturn().getResponse().getContentAsString()).get("id");

        var reservationDTOtoUpdate = TestInputs.generateReservationNumber(
                9, GameType.TWO_ON_TWO, 3L, TestInputs.LOGIN_ADMIN_DEFAULT.phoneNumber()
        );

        mvc.perform(put(BASE_URI + "/api/reservation/update/id=" + id)
                        .header(HttpHeaders.AUTHORIZATION, jwt)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Util.mapper.writeValueAsString(reservationDTOtoUpdate)))
                .andExpect(status().isOk());

        assertTrue(reservationRepository.findReservationById(Long.parseLong(id.toString())).isPresent());

        Util.logout(jwt, mvc);
    }

    @Test
    public void deleteReservation_Success() throws Exception {
        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);

        var reservationDTOtoCreate = TestInputs.generateReservationNumber(
                1, GameType.TWO_ON_TWO, 4L, TestInputs.LOGIN_ADMIN_DEFAULT.phoneNumber()
        );

        var created = Util.createReservation(reservationDTOtoCreate, jwt, mvc);
        created.andExpect(status().isOk());

        var id = Util.mapper.readTree(created.andReturn().getResponse().getContentAsString()).get("id");

        mvc.perform(delete(BASE_URI + "/api/reservation/delete/id=" + id)
                        .header(HttpHeaders.AUTHORIZATION, jwt)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        assertFalse(reservationRepository.findReservationById(Long.parseLong(id.toString())).isPresent());

        Util.logout(jwt, mvc);
    }

    @Test
    public void deleteReservation_OfOtherUser_Fail() throws Exception {
        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);

        var reservationDTOtoCreate = TestInputs.generateReservationNumber(
                5, GameType.TWO_ON_TWO, 4L, TestInputs.LOGIN_ADMIN_DEFAULT.phoneNumber()
        );

        var created = Util.createReservation(reservationDTOtoCreate, jwt, mvc);
        created.andExpect(status().isOk());

        Util.logout(jwt, mvc);

        var jwt2 = Util.loginAndGetJwt(TestInputs.LOGIN_USER_DEFAULT, mvc);

        var id = Util.mapper.readTree(created.andReturn().getResponse().getContentAsString()).get("id");

        mvc.perform(delete(BASE_URI + "/api/reservation/delete/id=" + id)
                        .header(HttpHeaders.AUTHORIZATION, jwt2)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());

        Util.logout(jwt2, mvc);

    }


}
