package com.august.courts.rest;

import com.august.courts.controller.dto.CourtDTO;
import com.august.courts.repositories.CourtRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
//@Rollback(value = true)
public class CourtTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private CourtRepository courtRepository;

    private static final String BASE_URI = "http://localhost:8080";


    @Test
    public void createCourt_AsAdmin_Success() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);

        var res = Util.createCourt("GRASS", jwt, mvc, 5L);
        res.andExpect(status().isOk());

        Util.logout(jwt, mvc);
    }

    @Test
    public void createCourt_AsAdmin_Fail() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);

        var res = Util.createCourt("GRASS", jwt, mvc, 1L);
        res.andExpect(status().isBadRequest());

        Util.logout(jwt, mvc);
    }

    @Test
    public void createCourt_AsUser_Fail() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_USER_DEFAULT, mvc);

        var res = Util.createCourt("GRASS", jwt, mvc, 6L);
        res.andExpect(status().isForbidden());

        Util.logout(jwt, mvc);
    }

    @Test
    public void getAllCourts_Success() throws Exception {

        int before = courtRepository.findAll().size();

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_USER_DEFAULT, mvc);

        var res = mvc.perform(get("/api/court/all")
                .header(HttpHeaders.AUTHORIZATION, jwt)
                .contentType(MediaType.APPLICATION_JSON));

        res.andExpect(status().isOk());
        assertEquals(
                before,
                Util.mapper.readTree(res.andReturn().getResponse().getContentAsString()).size()
        );

        Util.logout(jwt, mvc);
    }

    @Test
    public void getCourtById_Success() throws Exception {
        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_USER_DEFAULT, mvc);

        var res = mvc.perform(get("/api/court/get/id=1")
                .header(HttpHeaders.AUTHORIZATION, jwt)
                .contentType(MediaType.APPLICATION_JSON));

        res.andExpect(status().isOk());
        assertTrue(res.andReturn().getResponse().getContentAsString().contains("Northern court"));

        Util.logout(jwt, mvc);

    }

    @Test
    public void getCourtById_Fail() throws Exception {
        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_USER_DEFAULT, mvc);

        mvc.perform(get("/api/court/get/id=0")
                        .header(HttpHeaders.AUTHORIZATION, jwt)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        Util.logout(jwt, mvc);

    }

    @Test
    public void updateCourt_AsAdmin_Success() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);
        Util.createCourt("GRASS", jwt, mvc, 7L);
        var toBeUpdatedValues = new CourtDTO(
                7L,
                "Test_Court",
                "CLAY"
        );


        mvc.perform(put("/api/court/update/id=7")
                        .header(HttpHeaders.AUTHORIZATION, jwt)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Util.mapper.writeValueAsString(toBeUpdatedValues)))
                .andExpect(status().isOk());

        var updatedCourt = courtRepository.findCourtById(7L);
        assertTrue(updatedCourt.isPresent());
        assertEquals("CLAY", updatedCourt.get().getSurface().getSurfaceName());

        Util.logout(jwt, mvc);
    }

    @Test
    public void updateCourt_NonExistentSurface_Fail() throws Exception {
        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);
        Util.createCourt("GRASS", jwt, mvc, 8L);
        var toBeUpdatedValues = new CourtDTO(
                8L,
                "Test_Court_2",
                "HHH"
        );


        mvc.perform(put("/api/court/update/id=8")
                        .header(HttpHeaders.AUTHORIZATION, jwt)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Util.mapper.writeValueAsString(toBeUpdatedValues)))
                .andExpect(status().isBadRequest());

        Util.logout(jwt, mvc);

    }

    @Test
    public void deleteCourt_AsAdmin_Success() throws Exception {
        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);
        Util.createCourt("GRASS", jwt, mvc, 9L);

        var before = courtRepository.findAll().size();

        var res = mvc.perform(delete("/api/court/delete/id=9")
                .header(HttpHeaders.AUTHORIZATION, jwt)
                .contentType(MediaType.APPLICATION_JSON));

        res.andExpect(status().isOk());
        assertEquals(
                before - 1,
                courtRepository.findAll().size()
        );

        Util.logout(jwt, mvc);
    }

    @Test
    public void deleteCourt_AsAdmin_Fail() throws Exception {
        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);
        var before = courtRepository.findAll().size();

        var res = mvc.perform(delete("/api/court/delete/id=10")
                .header(HttpHeaders.AUTHORIZATION, jwt)
                .contentType(MediaType.APPLICATION_JSON));

        res.andExpect(status().isBadRequest());
        assertEquals(
                before,
                courtRepository.findAll().size()
        );

        Util.logout(jwt, mvc);
    }

    @Test
    public void deleteCourt_AsUser_Fail() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_USER_DEFAULT, mvc);
        Util.createCourt("GRASS", jwt, mvc, 11L);

        mvc.perform(delete("/api/court/delete/id=11")
                        .header(HttpHeaders.AUTHORIZATION, jwt)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());

        Util.logout(jwt, mvc);

    }


}

