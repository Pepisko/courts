package com.august.courts.rest;

import com.august.courts.controller.dto.SurfaceDTO;
import com.august.courts.domain.utils.SurfaceName;
import com.august.courts.repositories.SurfaceRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
//@Rollback(value = true)
public class SurfaceTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private SurfaceRepository surfaceRepository;

    private static final String BASE_URI = "http://localhost:8080";

    @Test
    public void createSurface_AsAdmin_Success() throws Exception {
        var surface = new SurfaceDTO(
                "TEST_GRASS",
                5.0
        );

        var before = surfaceRepository.findAll().size();

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);

        var res = Util.createSurface(surface, jwt, mvc);

        res.andExpect(status().isOk());
        assertEquals(before + 1, surfaceRepository.findAll().size());

        Util.logout(jwt, mvc);

    }

    @Test
    public void createSurface_AsUser_Fail() throws Exception {

        var surface = new SurfaceDTO(
                "SPECIAL_GRASS",
                10.0
        );

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_USER_DEFAULT, mvc);

        Util.createSurface(surface, jwt, mvc)
                .andExpect(status().isForbidden());

        Util.logout(jwt, mvc);

    }


    @Test
    public void getSurface_Success() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_USER_DEFAULT, mvc);

        var res = mvc.perform(get(BASE_URI + "/api/surface/get/name=CLAY")
                .header(HttpHeaders.AUTHORIZATION, jwt)
                .contentType(MediaType.APPLICATION_JSON));

        res.andExpect(status().isOk());
        assertTrue(res.andReturn().getResponse().getContentAsString().contains(SurfaceName.CLAY));

        Util.logout(jwt, mvc);
    }

    @Test
    public void getSurface_NonExistent_Fail() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_USER_DEFAULT, mvc);

        mvc.perform(get(BASE_URI + "/api/surface/get/name=BRAMBORA")
                        .header(HttpHeaders.AUTHORIZATION, jwt)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        Util.logout(jwt, mvc);


    }

    @Test
    public void getAll_Success() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_USER_DEFAULT, mvc);

        var res = mvc.perform(get(BASE_URI + "/api/surface/all")
                .header(HttpHeaders.AUTHORIZATION, jwt)
                .contentType(MediaType.APPLICATION_JSON));

        res.andExpect(status().isOk());
        assertTrue(res.andReturn().getResponse().getContentAsString().contains(SurfaceName.CLAY));
        assertTrue(res.andReturn().getResponse().getContentAsString().contains(SurfaceName.GRASS));


        Util.logout(jwt, mvc);


    }

    @Test
    public void updateSurface_AsAdmin_Success() throws Exception {
        var toBeUpdated = new SurfaceDTO(
                "ULTRA_SPECIAL_GRASS",
                17.0
        );

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);

        var res = mvc.perform(put(BASE_URI + "/api/surface/update/name=GRASS")
                .header(HttpHeaders.AUTHORIZATION, jwt)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Util.mapper.writeValueAsString(toBeUpdated)));

        res.andExpect(status().isOk());
        assertTrue(res.andReturn().getResponse().getContentAsString().contains(toBeUpdated.surfaceName()));
        assertTrue(res.andReturn().getResponse().getContentAsString().contains(String.valueOf(toBeUpdated.pricePerMinute())));

        Util.logout(jwt, mvc);

    }

    @Test
    public void updateSurface_AsUser_Fail() throws Exception {

        var toBeUpdated = new SurfaceDTO(
                "ULTRA_SPECIAL_CLAY",
                15.0
        );

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_USER_DEFAULT, mvc);

        mvc.perform(put(BASE_URI + "/api/surface/update/name=GRASS")
                        .header(HttpHeaders.AUTHORIZATION, jwt)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(Util.mapper.writeValueAsString(toBeUpdated)))
                .andExpect(status().isForbidden());

        Util.logout(jwt, mvc);

    }

    @Test
    public void deleteSurface_AsAdmin_Success() throws Exception {

        var surface = new SurfaceDTO(
                "TEST_SAND",
                5.5
        );

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);

        Util.createSurface(surface, jwt, mvc)
                .andExpect(status().isOk());


        var before = surfaceRepository.findAll().size();

        var res = mvc.perform(delete(BASE_URI + "/api/surface/delete/name=TEST_SAND")
                .header(HttpHeaders.AUTHORIZATION, jwt)
                .contentType(MediaType.APPLICATION_JSON));

        res.andExpect(status().isOk());
        assertEquals(before - 1, surfaceRepository.findAll().size());

        Util.logout(jwt, mvc);

    }

    @Test
    public void deleteSurface_NonExistent_Fail() throws Exception {

        var jwt = Util.loginAndGetJwt(TestInputs.LOGIN_ADMIN_DEFAULT, mvc);

        //Util.createSurface(surface, jwt, mvc);

        mvc.perform(delete(BASE_URI + "/api/surface/delete/name=TEST_FAIL_SAND")
                .header(HttpHeaders.AUTHORIZATION, jwt)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        Util.logout(jwt, mvc);

    }


}
