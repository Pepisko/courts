package com.august.courts.config.security;

import com.august.courts.controller.ErrorHandlerController;
import com.august.courts.domain.RoleName;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * Security configuration class.
 *
 * @author ali-bouali, Josef Augustín
 */
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
@Configuration
public class SecurityConfiguration {

    private final JwtAuthenticationFilter jwtAuthFilter;

    private final AuthenticationProvider authenticationProvider;

    private final AuthenticationEntryPoint authenticationEntryPoint;

    /**
     * Configure security.
     *
     * @param http HTTP security
     * @return security filter chain
     * @throws Exception if an error occurs
     */
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        return http
                .cors()
                .and()
                .csrf().disable()
                .authorizeHttpRequests()
                .requestMatchers("/api/auth/**").permitAll()
                .requestMatchers("/api/auth/update/**").hasAnyAuthority(RoleName.ADMIN)

                .requestMatchers("/api/reservation/create").hasAnyAuthority(RoleName.USER, RoleName.ADMIN)
                .requestMatchers("/api/reservation/all/**").hasAnyAuthority(RoleName.USER, RoleName.ADMIN)
                .requestMatchers("/api/reservation/**").hasAuthority(RoleName.ADMIN)

                .requestMatchers("/api/court/all").hasAnyAuthority(RoleName.USER, RoleName.ADMIN)
                .requestMatchers("/api/court/get/**").hasAnyAuthority(RoleName.USER, RoleName.ADMIN)
                .requestMatchers("/api/court/**").hasAuthority(RoleName.ADMIN)

                .requestMatchers("/api/surface/all").hasAnyAuthority(RoleName.USER, RoleName.ADMIN)
                .requestMatchers("/api/surface/get/**").hasAnyAuthority(RoleName.USER, RoleName.ADMIN)
                .requestMatchers("/api/surface/**").hasAuthority(RoleName.ADMIN)

                .requestMatchers("/api/user/update/self").hasAnyAuthority(RoleName.USER, RoleName.ADMIN)
                .requestMatchers("/api/user/all").hasAnyAuthority(RoleName.USER, RoleName.ADMIN)
                .requestMatchers("/api/user/**").hasAuthority(RoleName.ADMIN)


                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authenticationProvider(authenticationProvider)
                .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class)
                .exceptionHandling()
                .authenticationEntryPoint(authenticationEntryPoint)
                .and()
                .build();
    }
}

