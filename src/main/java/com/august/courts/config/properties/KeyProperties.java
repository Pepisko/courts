package com.august.courts.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Record for external jwt properties
 * @param secretKey secret key for jwt
 * @param expirationTimeSeconds expiration of one session
 */
@ConfigurationProperties(prefix = "secret")
public record KeyProperties(String secretKey, Integer expirationTimeSeconds) {
}
