package com.august.courts.domain.utils;

import lombok.AllArgsConstructor;

/**
 * Enum for game type (e.g. 1on1, 2on2) with multiplier for price calculation
 */
@AllArgsConstructor
public enum GameType {
    ONE_ON_ONE(1.0),
    TWO_ON_TWO(1.5);

    public final double multiplier;

}
