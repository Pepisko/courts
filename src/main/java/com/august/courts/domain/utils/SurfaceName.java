package com.august.courts.domain.utils;

/**
 * Class for surface names (Not needed, Only for better readability)
 */
public class SurfaceName {

    public static final String GRASS = "GRASS";

    public static final String CLAY = "CLAY";

    public static final String HARD = "HARD";

}

