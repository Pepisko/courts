package com.august.courts.domain.utils;

/**
 * Class for token types (Not needed, Only for better readability)
 */
public enum TokenType {
    BEARER
}
