package com.august.courts.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.util.List;

/**
 * A Court Entity (used also for json serialization)
 *
 * @author Josef Augustín .
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "court")
@SQLDelete(sql = "UPDATE court SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
public class Court {
    @Id
/*    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")*/
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @JsonIgnore
    private Boolean deleted = Boolean.FALSE;

    @JsonIgnore
    @OneToMany(mappedBy = "court", fetch = FetchType.LAZY)
    private List<Reservation> reservations;

    @ManyToOne(fetch = FetchType.EAGER)
    private Surface surface;

}
