package com.august.courts.domain;

public final class RoleName {

    public static final String USER = "USER";
    public static final String ADMIN = "ADMIN";

    private RoleName() {
    }

}
