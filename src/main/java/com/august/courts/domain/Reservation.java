package com.august.courts.domain;

import com.august.courts.domain.utils.GameType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.lang.NonNull;

import java.time.LocalDateTime;

/**
 * A Reservation Entity (used also for json serialization)
 *
 * @author Josef Augustín .
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "reservation")
@SQLDelete(sql = "UPDATE reservation SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NonNull
    @JsonProperty
    @Column(name = "from_time")
    private LocalDateTime fromTime;

    @NonNull
    @JsonProperty
    @Column(name = "to_time")
    private LocalDateTime toTime;

    @NonNull
    @Column(name = "price")
    private Double price;

    @NonNull
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @JsonIgnore
    private Boolean deleted = Boolean.FALSE;

    @NonNull
    @Enumerated(EnumType.STRING)
    @Column(name = "game_type")
    private GameType gameType;

    @JsonIgnore
    @NonNull
    @ManyToOne
    private ApplicationUser applicationUser;

    @JsonIgnoreProperties("reservations")
    @NonNull
    @ManyToOne
    private Court court;
}
