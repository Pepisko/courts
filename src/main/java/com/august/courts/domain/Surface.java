package com.august.courts.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.lang.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * A Court surface (Grass, Clay, etc.) (used also for json serialization)
 *
 * @author Josef Augustín .
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "surface")
@SQLDelete(sql = "UPDATE surface SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
public class Surface {
    @Id
    @SequenceGenerator(name = "surface_id_seq", sequenceName = "surface_id_seq", initialValue = 100)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "surface_id_seq")
    @Column(name = "id", nullable = false)
    private Long id;

    @NonNull
    @Column(name = "surface_name", unique = true)
    private String surfaceName;

    @NonNull
    @Column(name = "price_per_minute")
    private Double pricePerMinute;

    @JsonIgnore
    private Boolean deleted = Boolean.FALSE;

    @JsonIgnore
    @OneToMany(mappedBy = "surface")
    private List<Court> courts = new ArrayList<>();
}
