package com.august.courts.repositories;

import com.august.courts.domain.Surface;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SurfaceRepository extends JpaRepository<Surface, Long> {

    Optional<Surface> findBySurfaceName(String name);

    Integer deleteBySurfaceName(String name);

}
