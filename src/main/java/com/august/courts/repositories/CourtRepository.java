package com.august.courts.repositories;

import com.august.courts.domain.Court;
import com.august.courts.domain.utils.SurfaceName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CourtRepository extends JpaRepository<Court, Long> {

    Optional<Court> findCourtById(Long id);

    Integer deleteAllById(Long id);


}
