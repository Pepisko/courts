package com.august.courts.repositories;

import com.august.courts.domain.ApplicationUser;
import com.august.courts.domain.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {


    /**
     * Returns the number of reservations that overlap with the given time interval.
     *
     * @param fromTime the start of the time interval
     * @param toTime   the end of the time interval
     * @param courtId  the id of the court
     * @return the number of reservations that overlap with the given time interval
     */
    @Query("SELECT COUNT(r) FROM Reservation r" +
            " WHERE r.court.id = ?3 AND" +
            " ((r.fromTime <= ?1 AND r.toTime > ?1) OR" +
            " (r.fromTime < ?2 AND r.toTime >= ?2))")
    Integer countOverlappingReservations(LocalDateTime fromTime, LocalDateTime toTime, Long courtId);


    List<Reservation> getAllByCourt_IdOrderByCreatedAtAsc(Long courtId);

    List<Reservation> getAllByApplicationUser_PhoneNumber(String phoneNumber);

    List<Reservation> getAllByApplicationUser_PhoneNumberAndFromTimeAfter(String phoneNumber, LocalDateTime fromTime);


    Optional<Reservation> findReservationById(Long reservationId);

    void deleteByIdAndApplicationUser(Long reservationId, ApplicationUser applicationUser);

    List<Reservation> findAllByApplicationUser_PhoneNumber(String phoneNumber);

}
