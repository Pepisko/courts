package com.august.courts.repositories;

import com.august.courts.domain.ApplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ApplicationUserRepository extends JpaRepository<ApplicationUser, Long> {
    Optional<ApplicationUser> findApplicationUserByPhoneNumber(String phoneNumber);


    Boolean existsApplicationUserByPhoneNumber(String phoneNumber);

    Integer deleteByPhoneNumber(String phoneNumber);

}

