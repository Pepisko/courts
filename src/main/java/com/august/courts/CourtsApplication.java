package com.august.courts;

import com.august.courts.config.properties.KeyProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties({KeyProperties.class})
@SpringBootApplication
public class CourtsApplication {

    public static void main(String[] args) {
        var context = SpringApplication.run(CourtsApplication.class, args);



    }

}
