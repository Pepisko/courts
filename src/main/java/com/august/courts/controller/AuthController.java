package com.august.courts.controller;

import com.august.courts.controller.dto.ApplicationUserDTO;
import com.august.courts.controller.dto.LoginDTO;
import com.august.courts.domain.ApplicationUser;
import com.august.courts.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Rest Controller for Authentication (i.e. login, logout, register and updating own credentials)
 */
@AllArgsConstructor
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private final AuthService authService;

    /**
     * Register new user
     *
     * @param applicationUserDTO data transfer object for application user
     * @return created user
     */
    @PostMapping("register")
    public ResponseEntity<Void> register(@RequestBody @Validated @Valid ApplicationUserDTO applicationUserDTO) {
        authService.registerUser(applicationUserDTO);

        return ResponseEntity
                .ok()
                .build();
    }

    /**
     * Login user
     *
     * @param loginDTO data transfer object for login
     */
    @PostMapping("login")
    public ResponseEntity<Void> login(@RequestBody @Validated LoginDTO loginDTO) {
        var token = authService.login(loginDTO);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setBearerAuth(token);

        return ResponseEntity
                .ok()
                .headers(responseHeaders)
                .build();
    }

    /**
     * Logout user
     */
    @PostMapping("logout")
    public ResponseEntity<Void> logout() {
        authService.logout();

        return ResponseEntity
                .ok()
                .build();
    }

    /**
     * Update own credentials
     *
     * @param applicationUserDTO data transfer object for application user
     * @return updated user
     */
    @PutMapping("/update/self")
    public ResponseEntity<ApplicationUser> updateSelf(
            @RequestBody @Validated ApplicationUserDTO applicationUserDTO
    ) {
        var res = authService.updateSelf(applicationUserDTO);

        authService.logout();

        return ResponseEntity
                .ok()
                .body(res);
    }


}
