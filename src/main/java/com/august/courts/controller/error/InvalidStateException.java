package com.august.courts.controller.error;

public class InvalidStateException extends RuntimeException{

    public InvalidStateException(String message) {
        super(message);
    }
}
