package com.august.courts.controller.error;

public class TokenException extends RuntimeException{

    public TokenException(String message) {
        super(message);
    }
}
