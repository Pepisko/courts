package com.august.courts.controller.error;

import java.net.URI;

public final class ErrorConstants {

    public static final String PROBLEM_BASE_URL = "http://localhost:8080/problem";


    public static final URI DEFAULT_TYPE = URI.create(PROBLEM_BASE_URL + "/problem-with-message");
    public static final URI CONSTRAINT_VIOLATION_TYPE = URI.create(PROBLEM_BASE_URL + "/constraint-violation");
    public static final URI INVALID_PASSWORD_TYPE = URI.create(PROBLEM_BASE_URL + "/invalid-password");
    public static final URI INVALID_LOGIN_LENGTH = URI.create(PROBLEM_BASE_URL + "/invalid-login-length");
    public static final URI EMAIL_ALREADY_USED_TYPE = URI.create(PROBLEM_BASE_URL + "/email-already-used");
    public static final URI EMAIL_INVALID_TYPE = URI.create(PROBLEM_BASE_URL + "/email-invalid");
    public static final URI LOGIN_ALREADY_USED_TYPE = URI.create(PROBLEM_BASE_URL + "/login-already-used");
    public static final URI LOGIN_INVALID_TYPE = URI.create(PROBLEM_BASE_URL + "/login-invalid");

    public static final URI COURT_NOT_FOUND = URI.create(PROBLEM_BASE_URL + "/court-not-found");

    public static final URI RESERVATION_NOT_FOUND = URI.create(PROBLEM_BASE_URL + "/reservation-not-found");
    public static final URI OVERLAPPING_RESERVATION = URI.create(PROBLEM_BASE_URL + "/reservation-overlapping");
    public static final URI COURT_DOES_NOT_EXIST = URI.create(PROBLEM_BASE_URL + "/court-does-not-exist");
    public static final URI USER_DOES_NOT_EXIST = URI.create(PROBLEM_BASE_URL + "/user-not-existing");


    public static final URI SURFACE_NOT_FOUND = URI.create(PROBLEM_BASE_URL + "/surface-not-found");
}
