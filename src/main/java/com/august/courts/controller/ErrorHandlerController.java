package com.august.courts.controller;

import com.august.courts.controller.error.InvalidStateException;
import com.august.courts.controller.error.NotFoundException;
import com.august.courts.controller.error.TokenException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.jwt.JwtException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

/**
 * Controller advice for handling exceptions (to be correctly propagated to the client)
 */
@RequiredArgsConstructor
@ControllerAdvice
public class ErrorHandlerController {

    /**
     * Handle {@link JwtException} and return 401
     *
     * @param ex exception
     * @return response entity with message as body and status 401
     */
    @ExceptionHandler(JwtException.class)
    public ResponseEntity<String> handle(JwtException ex) {
        return ResponseEntity
                .status(401)
                .body(ex.getMessage());
    }

    /**
     * Handle {@link RuntimeException} and return 400
     *
     * @param ex exception
     * @return response entity with message as body and status 400
     */
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> handle(RuntimeException ex) {
        return ResponseEntity
                .badRequest()
                .body(ex.getMessage());
    }

}
