package com.august.courts.controller;

import com.august.courts.controller.dto.ApplicationUserDTO;
import com.august.courts.controller.dto.validation.PhoneNumber;
import com.august.courts.domain.ApplicationUser;
import com.august.courts.service.ApplicationUserService;
import com.august.courts.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * Rest Controller for ApplicationUser
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/user")
public class ApplicationUserController {

    private final ApplicationUserService applicationUserService;

    private final AuthService authService;

    /**
     * Create new user
     *
     * @param applicationUserDTO data transfer object for application user
     * @return created user
     */
    @PostMapping("/create")
    public ResponseEntity<ApplicationUser> create(
            @RequestBody @Validated ApplicationUserDTO applicationUserDTO
    ) {
        return ResponseEntity
                .ok()
                .body(applicationUserService.create(applicationUserDTO));
    }

    /**
     * Get all users
     *
     * @return collection of all users
     */
    @GetMapping("/all")
    public ResponseEntity<Collection<ApplicationUser>> getAll() {
        return ResponseEntity
                .ok()
                .body(applicationUserService.getAll());
    }

    /**
     * Get user by id
     *
     * @param applicationUserId id of user
     * @return user with given id
     */
    @GetMapping("/get/id={applicationUserId}")
    public ResponseEntity<ApplicationUser> getById(@PathVariable Long applicationUserId) {
        return ResponseEntity
                .ok()
                .body(applicationUserService.getById(applicationUserId));
    }

    /**
     * Get user by phone number
     *
     * @param phoneNumber phone number of user
     * @return user with given phone number
     */
    @GetMapping("/get/phone={phoneNumber}")
    public ResponseEntity<ApplicationUser> getByPhone(@PathVariable String phoneNumber) {
        return ResponseEntity
                .ok()
                .body(applicationUserService.getByPhoneNumber(phoneNumber));
    }

    /**
     * Update user by id
     *
     * @param applicationUserDTO data transfer object for application user
     * @param applicationUserId  id of user
     * @return updated user
     */
    @PutMapping("/update/id={applicationUserId}")
    public ResponseEntity<ApplicationUser> updateById(
            @RequestBody @Validated ApplicationUserDTO applicationUserDTO,
            @PathVariable Long applicationUserId
    ) {
        return ResponseEntity
                .ok()
                .body(applicationUserService.updateById(applicationUserId, applicationUserDTO));
    }

    /**
     * Update user by phone number
     *
     * @param applicationUserDTO data transfer object for application user
     * @param phoneNumber        phone number of user
     * @return updated user
     */
    @PutMapping("/update/phone={phoneNumber}")
    public ResponseEntity<ApplicationUser> updateByPhone(
            @RequestBody @Validated ApplicationUserDTO applicationUserDTO,
            @PathVariable @PhoneNumber @Validated String phoneNumber
    ) {
        return ResponseEntity
                .ok()
                .body(applicationUserService.updateUserByPhoneNumber(applicationUserDTO, phoneNumber));
    }

    /**
     * Update current user
     *
     * @param applicationUserDTO data from which is user updated
     * @return updated user
     */
    @PutMapping("/update/self")
    public ResponseEntity<ApplicationUser> updateSelf(
            @RequestBody @Validated ApplicationUserDTO applicationUserDTO
    ) {
        var res = applicationUserService.updateSelf(applicationUserDTO);

        authService.logout();

        return ResponseEntity
                .ok()
                .body(res);
    }

    /**
     * Delete user by phone number
     *
     * @param phoneNumber phone number of user
     * @return void
     */
    @DeleteMapping("/delete/phone={phoneNumber}")
    public ResponseEntity<Void> deleteByPhone(@PathVariable @PhoneNumber @Validated String phoneNumber) {
        applicationUserService.deleteByPhoneNumber(phoneNumber);

        return ResponseEntity
                .ok()
                .build();
    }

    /**
     * Delete self
     */
    @DeleteMapping("delete/self")
    public ResponseEntity<Void> deleteSelf() {
        applicationUserService.deleteSelf();

        authService.logout();

        return ResponseEntity
                .ok()
                .build();
    }


}
