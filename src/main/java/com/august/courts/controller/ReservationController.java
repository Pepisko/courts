package com.august.courts.controller;

import com.august.courts.controller.dto.ReservationCreateOutput;
import com.august.courts.controller.dto.ReservationDTO;
import com.august.courts.domain.Reservation;
import com.august.courts.service.ReservationService;
import com.nimbusds.jose.util.Pair;
import jakarta.persistence.Tuple;
import lombok.RequiredArgsConstructor;
import org.hibernate.sql.results.internal.TupleImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Rest Controller for Reservation
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/reservation")
public class ReservationController {

    private final ReservationService reservationService;

/**
     * Create new reservation
     *
     * @param reservationDTO data transfer object for reservation
     * @return created reservation id and price of reservation
     */
    @PostMapping("/create")
    public ResponseEntity<ReservationCreateOutput> createReservation(@RequestBody @Validated ReservationDTO reservationDTO) {
        var res = reservationService.create(reservationDTO);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new ReservationCreateOutput(res.getId(), res.getPrice()));
    }

    /**
     * Get all reservations by court id
     *
     * @return collection of all reservations by court id
     */
    @GetMapping("/all/courtId={courtId}")
    public ResponseEntity<List<Reservation>> getAllReservationsByCourtId(@PathVariable Long courtId) {
        return ResponseEntity
                .ok()
                .body(reservationService.getAllByCourt(courtId));
    }

    /**
     * Get all reservations
     *
     * @return collection of all reservations
     */
    @GetMapping("/all")
    public ResponseEntity<List<Reservation>> getAllReservations() {
        return ResponseEntity
                .ok()
                .body(reservationService.getAll());
    }

    /**
     * Get reservation by id
     *
     * @param phoneNumber phone number of reservation
     * @return collection of reservation of user with given phone number
     */
    @GetMapping("/all/phone={phoneNumber}/in-future={inFuture}")
    public ResponseEntity<List<Reservation>> getAllReservationsByPhoneNumber(
            @PathVariable String phoneNumber,
            @PathVariable Boolean inFuture
    ) {
        return ResponseEntity
                .ok()
                .body(reservationService.getAllByPhoneNumber(phoneNumber, inFuture));
    }

    /**
     * Get reservation by id
     *
     * @param reservationId id of reservation
     * @return reservation with given id
     */
    @PutMapping("/update/id={reservationId}")
    public ResponseEntity<Reservation> updateReservation(
            @PathVariable Long reservationId,
            @RequestBody @Validated ReservationDTO reservationDTO
    ) {
        return ResponseEntity
                .ok()
                .body(reservationService.update(reservationId, reservationDTO));
    }

    /**
     * Delete reservation by id
     *
     * @param reservationId id of reservation
     */
    @DeleteMapping("/delete/id={reservationId}")
    public ResponseEntity<Void> deleteReservation(@PathVariable Long reservationId) {
        reservationService.delete(reservationId);
        return ResponseEntity
                .ok()
                .build();
    }


}
