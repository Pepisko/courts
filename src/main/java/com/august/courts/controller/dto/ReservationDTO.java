package com.august.courts.controller.dto;

import com.august.courts.controller.dto.validation.PhoneNumber;
import com.august.courts.domain.utils.GameType;
import org.springframework.lang.NonNull;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Data transfer object for reservation
 *
 * @author Josef Augustín
 */
public record ReservationDTO(

        @NotNull(message = "time from when is court reserved is required")
        LocalDateTime fromTime,
        @NotNull(message = "time to when is court reserved is required")
        LocalDateTime toTime,
        @NotNull
        @PhoneNumber(message = "Phone number of user creating reservation must be 12 digits long and start with +")
        String phoneNumber,
        @NotNull(message = "Game type is required")
        GameType gameType,
        @NotNull(message = "Court id is required")
        Long courtId) {
}


