package com.august.courts.controller.dto;

/**
 * Output object when reservation is created
 *
 * @author Josef Augustín
 */
public record ReservationCreateOutput(Long id, Double price) {
}
