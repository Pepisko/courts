package com.august.courts.controller.dto;

import org.springframework.lang.NonNull;

import javax.validation.constraints.NotNull;

/**
 * Data transfer object for surface
 *
 * @author Josef Augustín
 */
public record SurfaceDTO(
        @NotNull(message = "Id is required")
        String surfaceName,
        @NotNull(message = "Price per minute is required")
        Double pricePerMinute

) {
}

