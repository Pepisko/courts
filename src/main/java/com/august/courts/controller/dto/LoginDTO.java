package com.august.courts.controller.dto;

import com.august.courts.controller.dto.validation.PhoneNumber;
import org.springframework.lang.NonNull;

import javax.validation.constraints.NotNull;

/**
 * Data transfer object for login
 *
 * @author Josef Augustín
 */
public record LoginDTO(
        @NotNull(message = "Phone number is required")
        @PhoneNumber(message = "Please login with valid phone number starting + and 12 digits long")
        String phoneNumber,

        @NotNull(message = "Password is required")
        String password) {
}
