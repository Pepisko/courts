package com.august.courts.controller.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Data transfer object for court
 *
 * @author Josef Augustín
 */
public record CourtDTO(
        @NotNull(message = "Id is required")
        Long id,
        @NotNull(message = "Name is required")
        @Size(min = 1, max = 50)
        String name,
        @NotNull(message = "Surface is required")
        String surfaceName) {
}
