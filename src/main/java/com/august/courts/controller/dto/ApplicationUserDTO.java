package com.august.courts.controller.dto;

import com.august.courts.controller.dto.validation.PhoneNumber;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collection;

/**
 * Data transfer object for application user
 *
 * @author Josef Augustín
 */
public record ApplicationUserDTO(

        @NotNull(message = "Phone number is required")
        @PhoneNumber(message = "Phone number must be 12 digits long and start with +")
        String phoneNumber,
        @NotNull(message = "Password is required")
        String password,
        String email,
        @NotNull(message = "Username is required")
        String username,
        @Nullable
        Collection<String> roleNames
) {
}
