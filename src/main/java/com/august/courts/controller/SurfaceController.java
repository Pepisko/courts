package com.august.courts.controller;


import com.august.courts.controller.dto.SurfaceDTO;
import com.august.courts.domain.Surface;
import com.august.courts.service.SurfaceService;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Rest Controller for Surface
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/surface")
public class SurfaceController {

    private final SurfaceService surfaceService;

    /**
     * Create new surface
     *
     * @param surfaceDTO data transfer object for surface
     * @return created surface
     */
    @PostMapping("/create")
    public ResponseEntity<Surface> createSurface(@RequestBody @Validated SurfaceDTO surfaceDTO) {
        return ResponseEntity
                .ok()
                .body(surfaceService.create(surfaceDTO));
    }

    /**
     * Get surface by id
     *
     * @param surfaceName name (e.g. GRASS, CLAY etc.) of surface
     * @return surface with given name
     */
    @GetMapping("get/name={surfaceName}")
    public ResponseEntity<Surface> getSurfaceByName(@PathVariable String surfaceName) {
        return ResponseEntity
                .ok()
                .body(surfaceService.getByName(surfaceName));
    }

    /**
     * Get all surfaces
     *
     * @return collection of all surfaces
     */
    @GetMapping("all")
    public ResponseEntity<List<Surface>> getAllSurfaces() {
        return ResponseEntity
                .ok()
                .body(surfaceService.getAll());
    }

    /**
     * Update surface
     *
     * @param surfaceDTO  data transfer object for surface
     * @param surfaceName name of surface to be updated
     * @return updated surface
     */
    @PutMapping("update/name={surfaceName}")
    public ResponseEntity<Surface> updateSurface(
            @RequestBody @Validated SurfaceDTO surfaceDTO,
            @PathVariable String surfaceName
    ) {
        return ResponseEntity
                .ok()
                .body(surfaceService.update(surfaceName, surfaceDTO));
    }

    /**
     * Delete surface
     *
     * @param surfaceName name of surface to be deleted
     * @return deleted surface
     */
    @DeleteMapping("delete/name={surfaceName}")
    public ResponseEntity<Surface> deleteSurface(@PathVariable String surfaceName) {
        surfaceService.delete(surfaceName);

        return ResponseEntity
                .ok()
                .build();
    }


}
