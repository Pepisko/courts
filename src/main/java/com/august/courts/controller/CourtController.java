package com.august.courts.controller;

import com.august.courts.controller.dto.CourtDTO;
import com.august.courts.domain.Court;
import com.august.courts.service.CourtService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * Rest Controller for Court
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/court")
public class CourtController {

    private final CourtService courtService;


    /**
     * Create new court
     *
     * @param courtDTO data transfer object for court
     * @return created court
     */
    @PostMapping("/create")
    public ResponseEntity<Court> create(@RequestBody @Validated CourtDTO courtDTO) {
        return ResponseEntity
                .ok()
                .body(courtService.create(courtDTO));
    }

    /**
     * Get all courts
     *
     * @return collection of all courts
     */
    @GetMapping("/all")
    public ResponseEntity<Collection<Court>> getAll() {
        return ResponseEntity
                .ok()
                .body(courtService.getAll());
    }

    /**
     * Get court by id
     *
     * @param courtId id of court
     * @return court with given id
     */
    @GetMapping("/get/id={courtId}")
    public ResponseEntity<Court> getById(@PathVariable Long courtId) {
        return ResponseEntity
                .ok()
                .body(courtService.getById(courtId));
    }

    /**
     * Get court by name
     *
     * @param courtDTO data transfer object for court
     * @param courtId  id of court to be updated
     * @return court with given name
     */
    @PutMapping("/update/id={courtId}")
    public ResponseEntity<Court> update(@RequestBody @Validated CourtDTO courtDTO, @PathVariable Long courtId) {
        return ResponseEntity
                .ok()
                .body(courtService.update(courtId, courtDTO));
    }

    /**
     * Delete court by id
     *
     * @param courtId id of court to be deleted
     */
    @DeleteMapping("/delete/id={courtId}")
    public ResponseEntity<Void> delete(@PathVariable Long courtId) {
        courtService.delete(courtId);
        return ResponseEntity
                .ok()
                .build();
    }


}
