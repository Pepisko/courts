package com.august.courts.service;

import com.august.courts.controller.dto.CourtDTO;
import com.august.courts.controller.error.NotFoundException;
import com.august.courts.domain.Court;
import com.august.courts.repositories.CourtRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

/**
 * Service class for managing courts.
 */
@RequiredArgsConstructor
@Transactional
@Service
public class CourtService {

    private final CourtRepository courtRepository;

    private final SurfaceService surfaceService;

    /**
     * create new court
     *
     * @param courtDTO court data
     */
    public Court create(CourtDTO courtDTO) {
        if (courtRepository.existsById(courtDTO.id()))
            throw new IllegalArgumentException("Court with id " + courtDTO.id() + " already exists");

        var court = new Court();
        fillCourt(courtDTO, court);

        return courtRepository.save(court);
    }

    /**
     * get all courts
     *
     * @return list of courts
     */
    public Collection<Court> getAll() {
        return courtRepository.findAll();
    }

    /**
     * get court by id
     *
     * @param courtId court id
     * @return court
     */
    public Court getById(Long courtId) {
        return courtRepository.findById(courtId)
                .orElseThrow(() -> new NotFoundException("Court not found"));
    }

    /**
     * update court
     *
     * @param id       court id
     * @param courtDTO court data
     * @return updated court
     */
    public Court update(Long id, CourtDTO courtDTO) {
        var toUpdate = courtRepository.findCourtById(id)
                .orElseThrow(() -> new NotFoundException("Court not found"));
        fillCourt(courtDTO, toUpdate);

        return courtRepository.save(toUpdate);
    }

    /**
     * delete court
     *
     * @param courtId court id
     */
    public void delete(Long courtId) {
        if (courtRepository.deleteAllById(courtId) == 0)
            throw new NotFoundException("Court not found");
    }

    private void fillCourt(CourtDTO courtDTO, Court courtToFill) {
        var surface = surfaceService.getByName(courtDTO.surfaceName());

//        Court.builder()
//                .id(courtDTO.id())
//                .name(courtDTO.name())
//                .surface(surface)
//                .build();

        courtToFill.setId(courtDTO.id());
        courtToFill.setName(courtDTO.name());
        courtToFill.setSurface(surface);
    }
}
