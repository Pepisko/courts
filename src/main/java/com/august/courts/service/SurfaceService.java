package com.august.courts.service;


import com.august.courts.controller.dto.SurfaceDTO;
import com.august.courts.controller.error.NotFoundException;
import com.august.courts.domain.Surface;
import com.august.courts.repositories.SurfaceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service class for managing surfaces.
 */
@RequiredArgsConstructor
@Service
public class SurfaceService {

    private final SurfaceRepository surfaceRepository;

    /**
     * create new surface
     *
     * @param surfaceDTO surface data
     */
    @Transactional
    public Surface create(SurfaceDTO surfaceDTO) {
        var surface = new Surface();
        fill(surfaceDTO, surface);

        return surfaceRepository.save(surface);
    }

    /**
     * get surface by name
     *
     * @param surfaceName surface name
     * @return surface
     */
    public Surface getByName(String surfaceName) {
        return surfaceRepository.findBySurfaceName(surfaceName)
                .orElseThrow(() -> new NotFoundException("Surface not found"));
    }

    /**
     * get all surfaces
     *
     * @return list of surfaces
     */
    public List<Surface> getAll() {
        return surfaceRepository.findAll();
    }

    /**
     * update surface by name
     *
     * @param surfaceName surface name
     * @param surfaceDTO  surface data
     */
    @Transactional
    public Surface update(String surfaceName, SurfaceDTO surfaceDTO) {
        var surface = getByName(surfaceName);
        fill(surfaceDTO, surface);

        return surfaceRepository.save(surface);
    }

    /**
     * delete surface by name
     *
     * @param surfaceName surface name
     */
    @Transactional
    public void delete(String surfaceName) {
        if (surfaceRepository.deleteBySurfaceName(surfaceName) == 0) {
            throw new NotFoundException("Surface not found");
        }
    }

    private void fill(SurfaceDTO surfaceDTO, Surface surfaceToFill) {
        surfaceToFill.setSurfaceName(surfaceDTO.surfaceName());
        surfaceToFill.setPricePerMinute(surfaceDTO.pricePerMinute());
    }


}
