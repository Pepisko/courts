package com.august.courts.service;

import com.august.courts.controller.dto.ReservationDTO;
import com.august.courts.controller.error.InvalidStateException;
import com.august.courts.controller.error.NotFoundException;
import com.august.courts.domain.ApplicationUser;
import com.august.courts.domain.Reservation;
import com.august.courts.domain.Role;
import com.august.courts.domain.RoleName;
import com.august.courts.repositories.CourtRepository;
import com.august.courts.repositories.ReservationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Service class for managing reservations.
 */
@Transactional
@RequiredArgsConstructor
@Service
public class ReservationService {

    private final ReservationRepository reservationRepository;

    private final CourtRepository courtRepository;

    private final ApplicationUserService applicationUserService;


    /**
     * create new reservation
     *
     * @param reservationDTO reservation data
     */
    public Reservation create(ReservationDTO reservationDTO) {
        var reservation = new Reservation();
        fill(reservationDTO, reservation, applicationUserService.getCurrentUser());
        return reservationRepository.save(reservation);
    }

    /**
     * get all reservations by court id
     *
     * @return list of reservations
     */
    public List<Reservation> getAllByCourt(Long courtId) {
        return reservationRepository.getAllByCourt_IdOrderByCreatedAtAsc(courtId);
    }

    /**
     * get all reservations
     *
     * @return list of reservations
     */
    public List<Reservation> getAll() {
        return reservationRepository.findAll();
    }

    /**
     * get all reservations by phone number
     *
     * @return list of reservations
     */
    public List<Reservation> getAllByPhoneNumber(String phoneNumber, boolean inFuture) {
        if (inFuture) {
            return reservationRepository.getAllByApplicationUser_PhoneNumberAndFromTimeAfter(
                    phoneNumber,
                    LocalDateTime.now()
            );
        } else {
            return reservationRepository.getAllByApplicationUser_PhoneNumber(phoneNumber);
        }
    }

    /**
     * update reservation by id
     *
     * @param reservationId reservation id
     * @return updated reservation
     */
    public Reservation update(Long reservationId, ReservationDTO reservationDTO) {
        var reservation = reservationRepository.findReservationById(reservationId)
                .orElseThrow(() -> new NotFoundException("Reservation not found"));
        var currentAppUser = applicationUserService.getCurrentUser();
        checkModifyOtherUserReservations(currentAppUser, reservation);

        fill(reservationDTO, reservation, currentAppUser);
        return reservationRepository.save(reservation);
    }

    private void checkModifyOtherUserReservations(ApplicationUser applicationUser, Reservation reservation) {
        if (!Objects.equals(applicationUser.getPhoneNumber(), reservation.getApplicationUser().getPhoneNumber())
                && !applicationUserService.getAuthorityNames(applicationUser).contains(RoleName.ADMIN)) {
            throw new InvalidStateException("Cannot modify other user's reservation");
        }
    }

    /**
     * delete reservation by id
     *
     * @param reservationId reservation id
     */
    public void delete(Long reservationId) {
        var currUser = applicationUserService.getCurrentUser();
        var toUpdate = reservationRepository.findReservationById(reservationId);

        checkModifyOtherUserReservations(
                currUser,
                toUpdate.orElseThrow(
                        () -> new NotFoundException("Reservation not found")
                ));

        reservationRepository.deleteByIdAndApplicationUser(reservationId, currUser);
    }

    public void fill(ReservationDTO reservationDTO,
                     Reservation reservationToFill,
                     ApplicationUser applicationUser
    ) {
        var court = courtRepository.findCourtById(reservationDTO.courtId())
                .orElseThrow(() -> new NotFoundException("Court not found"));

        if (reservationDTO.fromTime().isBefore(LocalDateTime.now())
                || reservationDTO.toTime().isBefore(reservationDTO.fromTime())) {
            throw new InvalidStateException("Reservation time is invalid");
        }

        var currUserAuthorities = applicationUserService.getCurrentUser().getRoles()
                .stream()
                .map(Role::getName)
                .collect(Collectors.toSet());

        if (!applicationUserService.getCurrentUser().getPhoneNumber().equals(reservationDTO.phoneNumber())
                && !currUserAuthorities.contains(RoleName.ADMIN)) {
            throw new InvalidStateException("You are not allowed to make reservation for other user");
        }

        checkReservationOverlapping(reservationDTO);

        var minutes = reservationDTO.fromTime()
                .until(reservationDTO.toTime(), ChronoUnit.MINUTES);

//        Reservation.builder()
//                .fromTime(reservationDTO.fromTime())
//                .toTime(reservationDTO.toTime())
//                .court(court)
//                .gameType(reservationDTO.gameType())
//                .price(
//                        court.getSurface().getPricePerMinute() * minutes * reservationToFill.getGameType().multiplier
//                )
//                .applicationUser(
//                        applicationUser
//                )
//                .createdAt(LocalDateTime.now())
//                .build();

        reservationToFill.setFromTime(reservationDTO.fromTime());
        reservationToFill.setToTime(reservationDTO.toTime());
        reservationToFill.setCourt(court);
        reservationToFill.setGameType(reservationDTO.gameType());
        reservationToFill.setPrice(
                court.getSurface().getPricePerMinute() * minutes * reservationToFill.getGameType().multiplier
        );
        reservationToFill.setApplicationUser(
                applicationUser
        );
        reservationToFill.setCreatedAt(LocalDateTime.now());
    }

    private void checkReservationOverlapping(ReservationDTO reservationDTO) {
        if (reservationRepository.countOverlappingReservations(
                reservationDTO.fromTime(),
                reservationDTO.toTime(),
                reservationDTO.courtId()) != 0) {
            throw new InvalidStateException("Reservation overlaps with another reservation");
        }
    }
}
