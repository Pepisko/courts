package com.august.courts.service;

import com.august.courts.controller.dto.ApplicationUserDTO;
import com.august.courts.controller.error.NotFoundException;
import com.august.courts.domain.ApplicationUser;
import com.august.courts.domain.Role;
import com.august.courts.domain.RoleName;
import com.august.courts.repositories.ApplicationUserRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service class for managing users.
 */
@RequiredArgsConstructor
@Service
public class ApplicationUserService {

    private final ApplicationUserRepository applicationUserRepository;

    private final RoleService roleService;

    private static final String PHONE_PATTERN = "^\\+\\d{12}$";

    @Getter
    private final PasswordEncoder passwordEncoder;


    /**
     * create user with roles
     *
     * @param applicationUserDTO user data
     * @param roleNames          roles
     * @throws IllegalArgumentException if user with such phone number already exists
     */
    public ApplicationUser create(ApplicationUserDTO applicationUserDTO, Collection<String> roleNames) {
        if (applicationUserRepository.existsApplicationUserByPhoneNumber(applicationUserDTO.phoneNumber())) {
            throw new IllegalArgumentException(
                    "User with phone number " + applicationUserDTO.phoneNumber() + " already exists"
            );
        }

        var appUser = new ApplicationUser();
        var authorities = roleNames.stream()
                .map(roleService::findByName)
                .collect(Collectors.toSet());
        fillAppUser(applicationUserDTO, appUser, authorities);

        return applicationUserRepository.save(appUser);
    }

    /**
     * create user without explicit roles
     *
     * @param applicationUserDTO user data (with nullable roles)
     */
    public ApplicationUser create(ApplicationUserDTO applicationUserDTO) {
        if (applicationUserDTO.roleNames() == null) {
            return create(applicationUserDTO, new HashSet<>());
        }
        return create(applicationUserDTO, applicationUserDTO.roleNames());
    }


    /**
     * get all users
     *
     * @return list of users
     */
    public List<ApplicationUser> getAll() {
        return applicationUserRepository.findAll();
    }

    /**
     * get user by id
     *
     * @param id user id
     * @return user
     * @throws NotFoundException if user not found
     */
    public ApplicationUser getById(Long id) {
        return applicationUserRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("User not found"));
    }

    /**
     * get current user from authentication context
     *
     * @return user
     * @throws NotFoundException if user not found
     */
    public ApplicationUser getCurrentUser() throws NotFoundException {
        var currentUser = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (currentUser == null || currentUser.getClass() != ApplicationUser.class) {
            throw new NotFoundException("User not found");
        }
        return (ApplicationUser) currentUser;
    }

    /**
     * get user by phone number
     *
     * @param phoneNumber user phone number
     * @return user
     * @throws NotFoundException if user not found
     */
    public ApplicationUser getByPhoneNumber(String phoneNumber) {
        return applicationUserRepository.findApplicationUserByPhoneNumber(phoneNumber)
                .orElseThrow(() -> new NotFoundException("User not found"));
    }

    /**
     * update user by phone number
     *
     * @param applicationUserDTO user data
     * @param phoneNumber        phone number of user to be updated
     * @return updated user
     */
    public ApplicationUser updateUserByPhoneNumber(ApplicationUserDTO applicationUserDTO, String phoneNumber) {
        return update(applicationUserDTO, getByPhoneNumber(phoneNumber));
    }

    /**
     * update user by id
     *
     * @param id                 id of user to be updated
     * @param applicationUserDTO user data
     * @return updated user
     */
    public ApplicationUser updateById(Long id, ApplicationUserDTO applicationUserDTO) {
        return update(applicationUserDTO, getById(id));
    }

    /**
     * get names of all authorities of user (roles)
     *
     * @param user user
     * @return list of role names
     */
    public List<String> getAuthorityNames(ApplicationUser user) {
        return user.getRoles().stream()
                .map(Role::getName)
                .collect(Collectors.toList());
    }

    /**
     * update current user
     *
     * @param applicationUserDTO user data
     * @return updated user
     * @throws IllegalArgumentException if user tries to assign himself admin role
     */
    public ApplicationUser updateSelf(ApplicationUserDTO applicationUserDTO) {
        var currUser = getCurrentUser();
        var authoritiesString = getAuthorityNames(currUser);


        if (applicationUserDTO.roleNames() != null
                && applicationUserDTO.roleNames().contains(RoleName.ADMIN)
                && !authoritiesString.contains(RoleName.ADMIN)) {
            throw new IllegalArgumentException("You can't assign yourself admin role");
        }

        return update(applicationUserDTO, currUser);
    }

    private ApplicationUser update(ApplicationUserDTO applicationUserDTO, ApplicationUser appUser) {
        if (applicationUserRepository.existsApplicationUserByPhoneNumber(applicationUserDTO.phoneNumber())) {
            throw new IllegalArgumentException(
                    "User with phone number " + applicationUserDTO.phoneNumber() + " already exists"
            );
        }

        var authorities = roleService.roleNamesToRoles(applicationUserDTO.roleNames());
        fillAppUser(applicationUserDTO, appUser, authorities);

        return applicationUserRepository.save(appUser);
    }

    private void fillAppUser(
            ApplicationUserDTO applicationUserDTO,
            ApplicationUser appUser,
            Set<Role> authorities
    ) {
        if (!applicationUserDTO.phoneNumber().matches(PHONE_PATTERN)) {
            throw new IllegalArgumentException("Phone number must start with + followed by 12 digits");
        }

//        ApplicationUser.builder()
//                .name(applicationUserDTO.username())
//                .passwordHash(passwordEncoder.encode(applicationUserDTO.password()))
//                .phoneNumber(applicationUserDTO.phoneNumber())
//                .email(applicationUserDTO.email())
//                .roles(authorities)
//                .build();

        appUser.setName(applicationUserDTO.username());
        appUser.setPasswordHash(passwordEncoder.encode(applicationUserDTO.password()));
        appUser.setPhoneNumber(applicationUserDTO.phoneNumber());
        appUser.setEmail(applicationUserDTO.email());
        appUser.setRoles(authorities);
    }

    /**
     * delete user by phone number
     *
     * @param phoneNumber user phone number
     */
    @Transactional
    public void deleteByPhoneNumber(String phoneNumber) {
        if (applicationUserRepository.deleteByPhoneNumber(phoneNumber) == 0) {
            throw new NotFoundException("User not found");
        }
    }

    /**
     * delete current user
     */
    @Transactional
    public void deleteSelf() {
        var currUser = getCurrentUser();
        deleteByPhoneNumber(currUser.getPhoneNumber());
    }

    /**
     * initialize default users DO NOT USE IN PRODUCTION
     */
    //TODO remove this in production
    @EventListener(ApplicationReadyEvent.class)
    public void createDefaultUsers() {
        var admin =
                new ApplicationUserDTO(
                        "+420000000001",
                        "admin",
                        "",
                        "admin",
                        Set.of("ADMIN")
                );

        var user =
                new ApplicationUserDTO(
                        "+420000000002",
                        "user",
                        "",
                        "user",
                        Set.of("USER")
                );


        try {
            create(admin);
            create(user);
        } catch (Exception ignored) {
        }
    }
}
