package com.august.courts.service;

import com.august.courts.config.security.JwtService;
import com.august.courts.controller.dto.ApplicationUserDTO;
import com.august.courts.controller.dto.LoginDTO;
import com.august.courts.domain.ApplicationUser;
import com.august.courts.domain.RoleName;
import com.august.courts.domain.Token;
import com.august.courts.domain.utils.TokenType;
import com.august.courts.repositories.RoleRepository;
import com.august.courts.repositories.TokenRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * Service class for managing authentication.
 */
@RequiredArgsConstructor
@Service
public class AuthService {

    private final RoleRepository roleRepository;
    private final TokenRepository tokenRepository;

    private final ApplicationUserService applicationUserService;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    /**
     * register user with role USER
     *
     * @param applicationUserDTO user data
     */
    @Transactional
    public void registerUser(ApplicationUserDTO applicationUserDTO) {
        applicationUserService.create(applicationUserDTO, Set.of(RoleName.USER));
    }

    /**
     * login user
     *
     * @param loginDTO username (e.g. phone number) and password
     */
    @Transactional
    public String login(LoginDTO loginDTO) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginDTO.phoneNumber(), loginDTO.password())
        );
        var appUser = applicationUserService.getByPhoneNumber(loginDTO.phoneNumber());

        var token = jwtService.generateToken(appUser);

        revokeAllUserTokens(appUser);
        saveUserToken(appUser, token);
        return token;
    }

    /**
     * logout current user
     */
    @Transactional
    public void logout() {
        var appUser = applicationUserService.getCurrentUser();
        revokeAllUserTokens(appUser);
    }

    private void saveUserToken(ApplicationUser user, String jwtToken) {
        Token.builder()
                .user(user)
                .token(jwtToken)
                .tokenType(TokenType.BEARER)
                .expired(false)
                .revoked(false)
                .build();
    }

    private void revokeAllUserTokens(ApplicationUser user) {
        tokenRepository.deleteAllByUser_Id(user.getId());
    }

    /**
     * update current user
     *
     * @param applicationUserDTO user data to be updated
     */
    @Transactional
    public ApplicationUser updateSelf(ApplicationUserDTO applicationUserDTO) {
        return applicationUserService.updateSelf(applicationUserDTO);
    }
}




