package com.august.courts.service;

import com.august.courts.controller.error.NotFoundException;
import com.august.courts.domain.Role;
import com.august.courts.repositories.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service class for managing roles (and authorities in one piece).
 */
@RequiredArgsConstructor
@Service
public class RoleService {

    private final RoleRepository roleRepository;

    /**
     * convert role names to role entities
     *
     * @param roleNames role names
     * @return set of roles
     */
    public Set<Role> roleNamesToRoles(Collection<String> roleNames) {
        if (roleNames != null) {
            return roleNames
                    .stream()
                    .map(
                            this::findByName)
                    .collect(Collectors.toSet());
        } else {
            return new HashSet<>();
        }
    }

    /**
     * get role by name
     *
     * @throws NotFoundException if role not found
     * @return one role
     */
    public Role findByName(String roleName) {
        return roleRepository.findByName(roleName)
                .orElseThrow(() -> new NotFoundException("Role not found"));
    }


}
