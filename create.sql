create sequence sequence_generator start with 1 increment by 50;
create sequence token_seq start with 1 increment by 50;

    create table application_user (
       id bigint not null,
        email varchar(255),
        name varchar(255),
        password_hash varchar(255),
        phone_number varchar(255),
        primary key (id)
    );

    create table court (
       id bigint not null,
        name varchar(255),
        surface_id bigint,
        primary key (id)
    );

    create table reservation (
       id bigint not null,
        from_time timestamp(6),
        game_type varchar(255),
        price float(53),
        to_time timestamp(6),
        application_user_id bigint,
        court_id bigint,
        primary key (id)
    );

    create table role (
       id varchar(255) not null,
        primary key (id)
    );

    create table surface (
       id bigint not null,
        price_per_minute float(53),
        surface_name varchar(255),
        primary key (id)
    );

    create table token (
       id integer not null,
        expired boolean not null,
        revoked boolean not null,
        token varchar(255),
        token_type varchar(255),
        user_id bigint,
        primary key (id)
    );

    create table user_roles (
       user_id bigint not null,
        role_id varchar(255) not null,
        primary key (user_id, role_id)
    );

    alter table if exists token 
       add constraint UK_pddrhgwxnms2aceeku9s2ewy5 unique (token);

    alter table if exists court 
       add constraint FKhm8dtei4oh99g9l1bdjxxstcd 
       foreign key (surface_id) 
       references surface;

    alter table if exists reservation 
       add constraint FKmgi0vwwfrdlno1dh7s2q0brcv 
       foreign key (application_user_id) 
       references application_user;

    alter table if exists reservation 
       add constraint FKpxnu37u6x3lp9jvebvujghn4e 
       foreign key (court_id) 
       references court;

    alter table if exists token 
       add constraint FK8b1vxxomgpd54ps5mcf675yu 
       foreign key (user_id) 
       references application_user;

    alter table if exists user_roles 
       add constraint FKrhfovtciq1l558cw6udg0h0d3 
       foreign key (role_id) 
       references role;

    alter table if exists user_roles 
       add constraint FKq0h6vpf3crn504yyep1hdv0vc 
       foreign key (user_id) 
       references application_user;
create sequence sequence_generator start with 1 increment by 50;
create sequence token_seq start with 1 increment by 50;

    create table application_user (
       id bigint not null,
        email varchar(255),
        name varchar(255),
        password_hash varchar(255),
        phone_number varchar(255),
        primary key (id)
    );

    create table court (
       id bigint not null,
        name varchar(255),
        surface_id bigint,
        primary key (id)
    );

    create table reservation (
       id bigint not null,
        from_time timestamp(6),
        game_type varchar(255),
        price float(53),
        to_time timestamp(6),
        application_user_id bigint,
        court_id bigint,
        primary key (id)
    );

    create table role (
       id varchar(255) not null,
        primary key (id)
    );

    create table surface (
       id bigint not null,
        price_per_minute float(53),
        surface_name varchar(255),
        primary key (id)
    );

    create table token (
       id integer not null,
        expired boolean not null,
        revoked boolean not null,
        token varchar(255),
        token_type varchar(255),
        user_id bigint,
        primary key (id)
    );

    create table user_roles (
       user_id bigint not null,
        role_id varchar(255) not null,
        primary key (user_id, role_id)
    );

    alter table if exists token 
       add constraint UK_pddrhgwxnms2aceeku9s2ewy5 unique (token);

    alter table if exists court 
       add constraint FKhm8dtei4oh99g9l1bdjxxstcd 
       foreign key (surface_id) 
       references surface;

    alter table if exists reservation 
       add constraint FKmgi0vwwfrdlno1dh7s2q0brcv 
       foreign key (application_user_id) 
       references application_user;

    alter table if exists reservation 
       add constraint FKpxnu37u6x3lp9jvebvujghn4e 
       foreign key (court_id) 
       references court;

    alter table if exists token 
       add constraint FK8b1vxxomgpd54ps5mcf675yu 
       foreign key (user_id) 
       references application_user;

    alter table if exists user_roles 
       add constraint FKrhfovtciq1l558cw6udg0h0d3 
       foreign key (role_id) 
       references role;

    alter table if exists user_roles 
       add constraint FKq0h6vpf3crn504yyep1hdv0vc 
       foreign key (user_id) 
       references application_user;
